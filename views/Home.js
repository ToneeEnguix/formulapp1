import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from "react-native";
import axios from "axios"
import { LinearGradient } from 'expo-linear-gradient';

export default function Home() {
  const [lastResults, setLastResults] = useState({})

  useEffect(() => {
    const getLastResults = async () => {
      try {
        const res = await axios.get("http://ergast.com/api/f1/current/last/results.json")
        setLastResults(res.data.MRData.RaceTable)
      } catch (err) {
        console.error(err)
      }
    }
    getLastResults()
  }, [])

  if (lastResults.Races) return (
    <View style={styles.wrapper}>
      <LinearGradient
        colors={['#ffffff', '#ffb7b7', '#ff9999']}
        style={styles.container}>
        <Text style={styles.title}>{lastResults.season} Season</Text>
        <View style={styles.subtitle}>
          <Text>{lastResults.Races[0].Circuit.circuitName}</Text>
          <Text>{lastResults.Races[0].date}</Text>
        </View>
        <Text style={styles.race}>{lastResults.Races[0].raceName}</Text>
        <View style={styles.summary}>
          <View>
            <Text style={styles.center}>1 {lastResults.Races[0].Results[1].Driver.code}</Text>
            {/* <Text style={styles.center}>2</Text> */}
          </View>
          <View>
            <Text style={styles.center}>2 {lastResults.Races[0].Results[0].Driver.code}</Text>
            {/* <Text style={styles.center}>1</Text> */}
          </View>
          <View>
            <Text style={styles.center}>3 {lastResults.Races[0].Results[2].Driver.code}</Text>
            {/* <Text style={styles.center}>3</Text> */}
          </View>
        </View>
      </LinearGradient>
    </View>
  );
  return null
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  container: {
    flex: 1,
    padding: "5%"
  },
  title: {
    fontSize: 25
  },
  subtitle: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "3%",
    marginTop: "2%"
  },
  race: {
    textAlign: "center",
    fontSize: 22
  },
  summary: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingRight: "15%",
    paddingLeft: "15%"
  },
  center: {
    textAlign: "center"
  }
})