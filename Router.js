import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Home from './views/Home';
import Pilots from './views/Pilots';
import Teams from './views/Teams';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Image } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import axios from "axios"

export default function Router() {
  const Stack = createBottomTabNavigator();

  const [constructors, setConstructors] = useState([{}])
  const [drivers, setDrivers] = useState([{}])

  useEffect(() => {
    const getTeamsData = async () => {
      try {
        const res = await axios.get("http://ergast.com/api/f1/2021/constructorStandings.json")
        setConstructors(res.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings)
      } catch (err) {
        console.error(err)
      }
    }
    const getDriversData = async () => {
      try {
        const res = await axios.get("http://ergast.com/api/f1/2021/driverStandings.json")
        setDrivers(res.data.MRData.StandingsTable.StandingsLists[0].DriverStandings)
      } catch (err) {
        console.error(err)
      }
    }
    getDriversData()
    getTeamsData()
  }, [])

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ focused, color, size }) =>
              focused ? (
                <Image
                  source={{ uri: 'https://logotyp.us/files/png/f1.png' }}
                  style={{
                    width: size,
                    height: size,
                    borderRadius: size
                  }}
                />
              ) : (
                <Entypo name="rocket" size={20} />
              )
          }}
        />
        <Stack.Screen
          name="Pilots"
          options={{
            tabBarIcon: ({ focused, color, size }) => (
              <Image
                source={{
                  uri:
                    'https://www.scottiesdesigns.com/wp-content/uploads/edd/2020/10/Racing-Helmet-SVG-Cricut-Silhouette.jpg'
                }}
                style={{
                  width: size,
                  height: size
                }}
              />
            )
          }}>
          {props => <Pilots {...props} drivers={drivers} />}
        </Stack.Screen>
        <Stack.Screen
          name="Teams"
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return (
                <Image
                  source={require('./assets/car2.png')}
                  style={{
                    width: size,
                    height: size
                  }}
                />
              );
            }
          }}>
          {props => <Teams {...props} constructors={constructors} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
