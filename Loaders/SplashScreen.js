import React, { useEffect } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Audio } from "expo-av";

export default function SplashScreen() {
  async function playSound() {
    // create playable sound
    const { sound } = await Audio.Sound.createAsync(
      require("../resources/sounds/f1_passing.wav")
    );
    await sound.playAsync();
    sound.unloadAsync();
  }

  useEffect(() => {
    playSound();
  }, []);

  return (
    <View style={styles.container}>
      <Text>Welcome to Formulapp1</Text>
      <Image
        style={{ height: 60, width: 60 }}
        source={{ uri: "https://logotyp.us/files/png/f1.png" }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
